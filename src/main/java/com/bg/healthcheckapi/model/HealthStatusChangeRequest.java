package com.bg.healthcheckapi.model;

import com.bg.healthcheckapi.eunms.HealthStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthStatusChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private HealthStatus healthStatus;
}
